//
//  ViewController.swift
//  BrowserApp
//
//  Created by Ismael Álvarez Bernal on 06/01/17.
//  Copyright © 2017 Ismael Álvarez Bernal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var urlEnter: UISearchBar!
    @IBOutlet var webView: UIWebView!
    @IBOutlet var loading: UIActivityIndicatorView!
    
    
    @IBAction func searchUrl(_ sender: AnyObject) {
        
        print(urlEnter.text)
        let url = URL(string: "https://\(urlEnter.text!)")
        
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
        self.view.endEditing(true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarSearchButtonClicked(_ searchBar : UISearchBar){
        print(urlEnter.text)
        let url = URL(string: "https://\(urlEnter.text!)")
        
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
        self.view.endEditing(true)
    }

    func webViewDidStartLoad (_: UIWebView){
        loading.startAnimating()
    }
    
    func webViewDidFinishLoad (_: UIWebView){
        loading.stopAnimating()
        
        let urlStringhttps = self.webView.request?.url?.absoluteString.replacingOccurrences(of: "https://", with: "")
        let urlStringhttp = urlStringhttps?.replacingOccurrences(of: "http://", with: "")
        self.urlEnter.text = urlStringhttps
        self.urlEnter.text = urlStringhttp
    }
}

